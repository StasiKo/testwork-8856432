<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ApiTest extends TestCase
{
    use DatabaseMigrations;

    protected $memberData = [
        'first_name' => 'test',
        'last_name' => 'test last',
        'phone' => 'phone test',
        'email' => 's_admin@mail.ru',
        'confirmEmail' => 's_admin@mail.ru',
        'password' => '12345678',
        'confirmPassword' => '12345678',
        'agree' => true,
        'subscribe' => true,
    ];
    /**
     * A test create route.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->call('GET', route('api.list'));
        $this->assertEquals(200, $response->status());
    }

    /**
     * A test create route.
     *
     * @return void
     */
    public function test_create()
    {
        $response = $this->call('POST', route('api.create'), $this->memberData);
        $decoded = json_decode($response->content(), true);
        $this->assertEquals('test', $decoded['first_name']);
    }

    /**
     * A test store route.
     *
     * @return void
     */
    public function test_update()
    {
        $data = $this->memberData;
        $data['first_name'] = 'test update';
        $this->call('POST', route('api.create'), $this->memberData);
        $this->call('PUT', route('api.update', ['id' => 1]), $data);
        $response = $this->call('GET', route('api.get', ['id' => 1]));
        $decoded = json_decode($response->content(), true);
        $this->assertEquals('test update', $decoded['first_name']);
    }

    /**
     * A test delete route.
     *
     * @return void
     */
    public function test_delete()
    {
        $this->call('POST', route('api.create'), $this->memberData);
        $this->call('DELETE', route('api.delete', ['id' => 1]));
        $response = $this->call('GET', route('api.get', ['id' => 1]));
        $this->assertEquals(404, $response->status());
    }
}
