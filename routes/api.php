<?php

use App\Http\Controllers\Api\V1\MemberController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1'], function() {
    Route::get('/member/list', [MemberController::class, 'list'])->name('api.list');
    Route::get('/member/{id}', [MemberController::class, 'get'])->name('api.get');
    Route::post('/member', [MemberController::class, 'create'])->name('api.create');
    Route::put('/member/{id}', [MemberController::class, 'update'])->name('api.update');
    Route::delete('/member/{id}', [MemberController::class, 'delete'])->name('api.delete');
});

